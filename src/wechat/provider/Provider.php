<?php
declare (strict_types=1);

namespace mark\wechat\provider;

use mark\http\Curl;

/**
 * Prpcrypt class
 *
 * 提供接收和推送给公众平台消息的加解密接口.
 */
class Provider {
    const PRE_AUTH_CODE_KEY = 'wechat_provider_pre_auth_code_key';
    const VERIFY_TICKET = 'wechat_provider_verify_ticket';
    const TOKEN = 'wechat_provider_token';
    const PRE_AUTH_CODE = 'wechat_provider_pre_auth_code';
    const ENCRYPT_MSG = 'wechat_provider_encrypt_msg';

    /**
     * 解析验证票据
     *
     *  授权流程
     * 1. 接收component_verify_ticket：
     *
     * [1]微信服务器每隔10分钟会向第三方的消息接收地址推送一次component_verify_ticket，拿到后需要在本地做好存储；
     * [2]微信第三方平台的消息是加密的（下图），需要进行解密才能获取需要的信息；
     * [3]接收并解密消息，代码如下:
     *
     * @param array  $ticket = array('signature' => '', 'timestamp' => '', 'nonce' => '', 'encrypt_type' => '','msg_signature' => '')
     * @param string $appid
     * @param string $token
     * @param string $key
     *
     * @return array
     */
    public static function component_verify_ticket(array $ticket, string $appid, string $token, string $key) {

        if (isset($ticket['signature']) && !empty($ticket['signature'])) {
            $signature = trim($ticket['signature']);
        } else {
            return array();
        }
        if (isset($ticket['timestamp']) && !empty($ticket['timestamp'])) {
            $timestamp = trim($ticket['timestamp']);
        } else {
            return array();
        }

        if (isset($ticket['nonce']) && !empty($ticket['nonce'])) {
            $nonce = trim($ticket['nonce']);
        } else {
            return array();
        }

        if (isset($ticket['encrypt_type']) && !empty($ticket['encrypt_type'])) {
            $encrypt_type = trim($ticket['encrypt_type']);
        } else {
            return array();
        }

        if (isset($ticket['msg_signature']) && !empty($ticket['msg_signature'])) {
            $msg_signature = trim($ticket['msg_signature']);
        } else {
            return array();
        }

// 这里是PDO 方式连接数据库
        // $pdo = new PDO("mysql:host=localhost;dbname=db","XXXX","XXXX");
        // $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        // $pdo->exec('set names utf8');

        $encodingAesKey = $key;
        // 接受POST 传过来的数据
        // $encryptMsg = file_get_contents("php://input");
        // $this->logResult('log.log', 'XML SHUJU'.$encryptMsg."\n");
        $pc = new BizMsgCrypt($token, $encodingAesKey, $appid);
        //转换为simplexml对象
        //$xmlResult = simplexml_load_string($encryptMsg);
        //输出xml节点名称和值
        //$encrypt = $xmlResult->Encrypt;

        //  这是提取XML 里面Encrypt 节点的内容 上面注释的 也是一种方式方式很多 都可以用
        $xml_tree = new \DOMDocument();
        $xml_tree->loadXML($encryptMsg);
        $array_e = $xml_tree->getElementsByTagName('Encrypt');
        $encrypt = $array_e->item(0)->nodeValue;

        $format = "<xml><ToUserName><![CDATA[toUser]]></ToUserName><Encrypt><![CDATA[%s]]></Encrypt></xml>";
        $from_xml = sprintf($format, $encrypt);
        // $this->logResult('log.log', $from_xml."\n\r");

        // 第三方收到公众号平台发送的消息
        $msg = '';
        $errCode = $pc->decryptMsg($msg_signature, $timestamp, $nonce, $from_xml, $msg);

        if ($errCode == 0) {
            print("解密后: " . $msg . "\n");
            $xml = new \DOMDocument();
            $xml->loadXML($msg);
            $array_e = $xml->getElementsByTagName('ComponentVerifyTicket');
            $component_verify_ticket = $array_e->item(0)->nodeValue;
            // 写到日志
            // file_put_contents('log.log', $component_verify_ticket."\n\r");
            // $this->logResult('log.log','解密后component_verify_ticket：'.$component_verify_ticket."\n\r");

            return $component_verify_ticket;
            // echo 'success';
        } else {
            // $this->logResult('log.log','解密后失败：'.$errCode."\n");
            print("解密后失败：" . $errCode . "<br/>");
        }

        // $encryptMsg = file_get_contents('php://input');
        $encryptMsg = $msg_signature;
        // $wxData = C("platform_setting");
        // $encodingAesKey = $wxData['encodingAesKey'];
        $encodingAesKey = $key;
        // $token = $wxData['token'];
        // $appId = $wxData['appId'];
        $appId = $appid;

        $Wxcrypt = new BizMsgCrypt($token, $encodingAesKey, $appId);
        // $postArr = ArrayTool::xml2array($encryptMsg);

        $postArr = json_decode(json_encode($encryptMsg), true);

        $format = "<xml><ToUserName><![CDATA[toUser]]></ToUserName><Encrypt><![CDATA[%s]]></Encrypt></xml>";
        $fromXml = sprintf($format, $postArr['Encrypt']);
        //第三方收到公众号平台发送的消息
        $msg = '';
        $errCode = $Wxcrypt->decryptMsg($msg_signature, $timestamp, $nonce, $fromXml, $msg); // 解密
        if ($errCode == 0) {
            // $param = ArrayTool::xml2array($msg);
            $param = json_decode(json_encode($msg), true);
            switch ($param['InfoType']) {
                case 'component_verify_ticket' :    // 授权凭证
                    $componentVerifyTicket = $param['ComponentVerifyTicket'];
                    // S('component_verify_ticket_' . $appId, $componentVerifyTicket);
                    break;
                case 'unauthorized' :               // 取消授权
                    break;
                case 'authorized' :                 // 授权
                    break;
                case 'updateauthorized' :           // 更新授权
                    break;
            }
        }
        // exit("success");

        return $param;
    }

    /**
     * 令牌（component_access_token）是第三方平台接口的调用凭据。令牌的获取是有限制的，每个令牌的有效期为 2 小时，请自行做好令牌的管理，在令牌快过期时（比如1小时50分），重新调用接口获取。
     * 如未特殊说明，令牌一般作为被调用接口的 GET 参数 component_access_token 的值使用
     *
     * @param string $appid  component_appid      第三方平台 appid
     * @param string $secret component_appsecret     第三方平台 appsecret
     * @param string $ticket component_verify_ticket     微信后台推送的 ticket
     *
     * @return array
     * @throws \Exception
     */
    public static function get_component_token(string $appid, string $secret, string $ticket) {
        $url = 'https://api.weixin.qq.com/cgi-bin/component/api_component_token';
        $result = Curl::getInstance()
            ->post($url, 'json')
            ->appendData('component_appid', $appid)
            ->appendData('component_appsecret', $secret)
            ->appendData('component_verify_ticket', $ticket)
            ->toArray();

        return $result;
    }

    /**
     * 预授权码（pre_auth_code）是第三方平台方实现授权托管的必备信息，每个预授权码有效期为 1800秒。需要先获取令牌才能调用。使用过程中如遇到问题，可在开放平台服务商专区发帖交流。
     *
     * @param string $appid component_appid    string    第三方平台 appid
     * @param string $token component_access_token    第三方平台component_access_token，不是authorizer_access_token
     *
     * @throws \Exception
     * @link https://developers.weixin.qq.com/doc/oplatform/Third-party_Platforms/api/component_access_token.html
     */
    public static function create_preauthcode(string $appid, string $token) {
        $url = 'https://api.weixin.qq.com/cgi-bin/component/api_create_preauthcode?component_access_token=' . $token;
        // $url = 'https://api.weixin.qq.com/cgi-bin/component/api_create_preauthcode';
        $result = Curl::getInstance()
            ->post($url, 'json')
            ->appendData('component_access_token', $token)
            ->appendData('component_appid', $appid)
            ->toArray();
        return $result;
    }

    /**
     * 4.使用授权码换取公众号的接口调用凭据和授权信息：
     *
     * @param string $component_access_token
     * @param string $component_appid
     * @param string $authorization_code
     *
     * @return array
     * @throws \Exception
     */
    public static function query_auth(string $component_access_token, string $component_appid, string $authorization_code) {
        //1. 使用授权码换取公众号的接口调用凭据和授权信息
        $uri = "https://api.weixin.qq.com/cgi-bin/component/api_query_auth?component_access_token=" . $component_access_token;

        $result = Curl::getInstance()
            ->post($uri, 'json')
            ->appendData('component_access_token', $component_access_token)
            ->appendData('component_appid', $component_appid)
            ->appendData('authorization_code', $authorization_code)
            ->toArray();

        if (isset($result['authorization_info']) && !empty($result['authorization_info'])) {
            return $result;
        }

        return $result;
    }

}