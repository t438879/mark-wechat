<?php

declare (strict_types=1);

namespace mark\wechat\notice;

use think\facade\Cache;
use think\facade\Request;
use think\facade\Session;
use mark\http\Curl;
use mark\system\Os;
use Exception;
use think\facade\Log;

/**
 * Class Notice
 *
 * @package mark\wechat\notice
 */
final class Notice {
    protected $appid;
    protected $appsecret;

    /**
     * Notice constructor.
     *
     * @param null $appid
     * @param null $appsecret
     */
    public function __construct($appid = null, $appsecret = null) {
        if (!empty($appid) && !empty($appsecret)) {
            $this->appid = $appid;
            $this->appsecret = $appsecret;
        } else {
            $this->appid = Config('auth.wechat.appid');
            $this->appsecret = Config('auth.wechat.secret');
        }
    }

    /**
     * 发送模板消息
     *
     * @param string $json_template
     *
     * @return mixed
     */
    public function send(string $json_template) {
        // 模板消息
        $url = 'https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=' . $this->access_token();
        $jsonResult = $this->curl_post($url, urldecode($json_template));
        // $jsonResult = $this->curl_post($url, $json_template);
        if (empty($jsonResult)) {
            return '';
        }
        $result = json_decode($jsonResult, true);
        // Log::info('Notice::Send()' . json_encode(urldecode($json_template), JSON_UNESCAPED_UNICODE));
        // Log::error('Notice::Result()' . json_encode($result, JSON_UNESCAPED_UNICODE));

        try {
            //若未登录则无法获取Uid，临时性固定写成12
            // Notice:send(Exception)SQLSTATE[23000]: Integrity constraint violation: 1048 Column "uid" cannot be null
            $post['uid'] = Session::get('uid', 12);

            // $post["wxid"] = $this->openid;
            $post['wxid'] = json_decode($json_template)->touser;
            // $post["chattype"] = $this->chattype;
            // $post["chattype"] = json_decode($json_template)->chattype;
            $post['msgid'] = $result['msgid'];
            $post['errcode'] = $result['errcode'];
            $post['errmsg'] = $result['errmsg'];
            $post['message'] = urldecode($json_template);
            $post['time'] = time();

            if ($result['errcode'] == 0) {
                $post['status'] = 1;
            } else {
                $post['status'] = 2;
            }
            // Db::name('message_template')->insertGetId($post);
        } catch (Exception $exception) {

        }

        return $result;
    }

    /**
     * 获取微信 Access_Token
     *
     * @return mixed|string
     */
    private function access_token() {
        // 获取access_token
        $token_url = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=' . $this->appid . '&secret=' . $this->appsecret;
        if (Cache::has('access_token') && !empty(Cache::get('access_token'))) {
            return Cache::get('access_token');
        }

        $token = Curl::getInstance()
            ->post($token_url)
            ->toArray();
        if (!empty($token) && !empty($token['access_token'])) {
            Cache::set('access_token', $token['access_token'], 7000);
            return $token['access_token'];
        }

        $json_token = $this->curl_post($token_url);
        if (!empty($json_token)) {
            $token = json_decode($json_token, true);
            if (!empty($token) && !empty($token['access_token'])) {
                Cache::set('access_token', $token['access_token'], 7000);
                return $token['access_token'];
            }
        }

        return '';
    }

    /**
     * @param       $url
     * @param array $data
     *
     * @return mixed
     * curl请求
     */
    private function curl_post($url, $data = array()) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        // POST数据
        curl_setopt($ch, CURLOPT_POST, 1);
        // 把post的变量加上
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $output = curl_exec($ch);
        curl_close($ch);

        return $output;
    }

    /**
     * 运行预警
     *
     * @param string $wxid    微信OpenID
     * @param string $first
     * @param string $source  来源
     * @param int    $time    时间
     * @param string $content 内容
     * @param string $url     Url
     * @param string $remark  备注
     *
     * @return mixed|string
     */
    public static function runevent(string $wxid, string $first, string $source, $time = 0, $content = '', $url = '', $remark = '') {
        $notice = new Notice(Config('auth.wechat.appid'), Config('auth.wechat.secret'));
        if ($time == 0) {
            $time = time();
        }

        return $notice->send(self::eventMsg($wxid, $first, $source, $time, $content, $url, $remark));
    }

    /**
     * 运行预警消息内容
     *
     * @param string $wxid
     * @param string $first
     * @param string $source
     * @param int    $time
     * @param string $content
     * @param string $url
     * @param string $remark
     *
     * @return string
     * @throws Exception
     */
    private static function eventMsg(string $wxid, string $first = '', string $source = '', $time = 0, $content = '', $url = '', $remark = '') {
        if (empty($first)) {
            $first = '运行预警';
        }
        if (empty($source)) {
            $source = '无效频道';
        }
        if ($time === 0) {
            $time = time();
        }
        if (empty($content)) {
            $content = Os::getOs(true, 'string') . ' ' . Os::getBrand(true, 'string') . Os::getBrowser(true, 'string');
        }
        if (empty($url)) {
            $url = Request::url(true);
        }
        if (empty($remark)) {
            $remark = self::taobao();
        }
        $template = array(
            'touser' => $wxid,
            'template_id' => 'RTgP0JuE-OecgnurtQ4fVPL8vUNurPW5jubkKZpWTio',
            'url' => $url,
            'topcolor' => '#FF0000',
            'data' => array(
                'first' => array('value' => urlencode($first), 'color' => '#DC3545'),
                'keyword1' => array('value' => urlencode($source), 'color' => '#FD7E14'),
                'keyword2' => array('value' => date('Y-m-d H:i:s', $time)),
                'keyword3' => array('value' => urlencode($content), 'color' => '#343A40'),
                'remark' => array('value' => urlencode($remark))),
            'chattype' => '运行报警'
        );

        // return json_encode($template, JSON_UNESCAPED_UNICODE);
        $msg = json_encode($template);
        if (is_string($msg)) {
            return $msg;
        }

        return '';
    }

    /**
     * 淘宝IP
     *
     * @return string
     * @throws Exception
     */
    private static function taobao() {
        $ip = Os::getIpvs();
        $result = Curl::getInstance()
            ->get('http://ip.taobao.com/service/getIpInfo.php?ip=' . $ip, 'json')
            ->toArray();

        if (empty($result)) {
            Log::error('TaoBao IP Info Error');
            return self::juhe();
        }

        if ($result['code'] == 0) {
            return implode('_', $result['data']) . ' ' . $ip;
        }

        Log::error('TaoBao IP Info Warning ' . json_encode($result, JSON_UNESCAPED_UNICODE));
        return self::juhe();
    }

    /**
     * 聚合IP
     *
     * @return string
     * @throws Exception
     */
    private static function juhe() {
        $ip = Os::getIpvs();
        $result = Curl::getInstance()
            ->get('http://apis.juhe.cn/ip/ipNew?ip=' . $ip . '&key=f242a7b62e202745e0964a877f3657de', 'json')
            ->toArray();
        if (empty($result)) {
            Log::error('juhe IP Info Error');
            return $ip;
        }

        if ($result['resultcode'] == 200) {
            return implode('_', $result['result']) . ' ' . $ip;
        }

        Log::error('juhe IP Info Warning ' . json_encode($result, JSON_UNESCAPED_UNICODE));

        return $ip;
    }

}